# Itunes search Demo #

Itunes Search demo which allows you search for albums and once you click on it, you can see the details of the selected album (tracks, release date, price, etc.)

The project supports iOS 8 or newer, runs on iPhone and iPad and is following MVVM pattern which moves most of the business logic to the ViewModel file related with the ViewController

The app includes some examples of UnitTest and UITest that covers more than 60 percent of the code written.
