//
//  Album.swift
//  iTunesSearch
//

import Foundation

struct Album {
    let artistName: String
    let albumName: String
    let thumbnail: URL
    let releaseDate: Date
    let price: NSNumber
    let currencyCode: String
    var tracks: [Track]
    
    var priceString: String {
        let formatter = NumberFormatter()
        formatter.currencyCode = currencyCode
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        if #available(iOS 9.0, *) {
            formatter.numberStyle = .currencyAccounting
        } else {
            // Fallback on earlier versions
            formatter.numberStyle = .currency
        }
        return formatter.string(from: price) ?? LocalizableConstants.NotAvailable.rawValue.localized()
    }

    struct Keys {
        static let artistName = "artistName"
        static let  albumName = "collectionName"
        static let  thumbnail = "artworkUrl100"
        static let  releaseDate = "releaseDate"
        static let  collectionPrice = "collectionPrice"
        static let  currencyCode = "currency"
    }
}
