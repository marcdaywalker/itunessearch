//
//  AlbumCell.swift
//  iTunesSearch
//

import UIKit

class AlbumCell: UITableViewCell {
    @IBOutlet weak var albumImage: UIImageView!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var albumLabel: UILabel!
}
