//
//  AlbumCollectionCell.swift
//  iTunesSearch
//
//  Created by Madriz on 23/4/17.
//  Copyright © 2017 MMApps. All rights reserved.
//

import UIKit

class AlbumCollectionCell: UICollectionViewCell {
    @IBOutlet weak var albumImage: UIImageView!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var albumLabel: UILabel!
}
