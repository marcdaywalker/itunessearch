//
//  Date.swift
//  iTunesSearch
//

import Foundation

extension Date {
    func dateToMediumString () -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter.string(from: self)
    }
}
