//
//  DetailViewController.swift
//  iTunesSearch
//

import UIKit

class DetailViewController: UITableViewController {

    var headerView: HeaderView?
    var viewModel: DetailViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView = Bundle.main.loadNibNamed("HeaderView", owner: nil, options: nil)?.first as? HeaderView
        self.tableView.tableHeaderView = headerView
        
        self.tableView.register(UINib(nibName: "TrackCell", bundle: nil), forCellReuseIdentifier: "trackCell")
        viewModel?.buildDetails()
        viewModel?.search()
    }
    
    // MARK: - Table View
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.rows ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "trackCell", for: indexPath) as? TrackCell else {
            return UITableViewCell()
        }
        cell.trackNumber?.text = viewModel?.trackNumber(row: indexPath.row)
        cell.trackName?.text = viewModel?.trackName(row: indexPath.row)
        cell.trackPreview = viewModel?.trackPreview(row: indexPath.row)
        return cell
    }
}

