//
//  DetailViewModel.swift
//  iTunesSearch
//

import Foundation

class DetailViewModel {
    weak var viewController: DetailViewController?
    private var album: Album 
    
    init (album: Album) {
        self.album = album
    }
    
    var rows: Int {
        return album.tracks.count
    }
    
    func buildDetails () {
        viewController?.navigationItem.title = "\(album.artistName) - \(album.albumName)"
        viewController?.headerView?.logo.imageFromURL(url: album.thumbnail)
        viewController?.headerView?.background.imageFromURL(url: album.thumbnail)
        viewController?.headerView?.artistName.text = album.artistName
        viewController?.headerView?.albumName.text = album.albumName
        viewController?.headerView?.albumDate.text = album.releaseDate.dateToMediumString()
        viewController?.headerView?.albumPrice.text = album.priceString
    }
    
    func search() {
        ServicesManager.search(text: "\(album.artistName) \(album.albumName)", type: .song) { [unowned self] (err, dic) in
            if let msg = err {
                self.viewController?.showMessage(message: msg)
            } else {
                guard let json = dic else {
                    self.viewController?.showMessage(message: LocalizableConstants.NoResultsFound.rawValue.localized())
                    return
                }
                
                if let jsonArray = json["results"] as? [[String: AnyObject]] {
                    for json in jsonArray {
                        guard let album = self.parseSong(json: json) else {
                            return
                        }
                        self.album.tracks.append(album)
                    }
                    OperationQueue.main.addOperation({
                        self.viewController?.hideActivityIndicator()
                        self.viewController?.tableView.reloadData()
                    })
                } else {
                    self.viewController?.showMessage(message: LocalizableConstants.NoResultsFound.rawValue.localized())
                }
                
            }
        }
    }
    
    private func parseSong (json: [String: AnyObject]) -> Track? {
        guard let title = json[Track.Keys.trackName] as? String,
            let number = json[Track.Keys.trackNumber] as? Int,
            let previewString = json[Track.Keys.previewUrl] as? String,
            let previewURL = URL(string: previewString) else {
                return nil
        }
        let track = Track(title: title, previewURL: previewURL, trackNumber: number)
        return track
    }
    
    func trackName (row: Int) -> String {
        var sorted = album.tracks.sorted(by: { return $0.trackNumber <= $1.trackNumber })
        return sorted[row].title
    }
    
    func trackNumber (row: Int) -> String {
        var sorted = album.tracks.sorted(by: { return $0.trackNumber <= $1.trackNumber })
        return String(sorted[row].trackNumber)
    }
    
    func trackPreview (row: Int) -> URL {
        var sorted = album.tracks.sorted(by: { return $0.trackNumber <= $1.trackNumber })
        return sorted[row].previewURL
    }
}
