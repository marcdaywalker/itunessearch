//
//  HeaderView.swift
//  iTunesSearch
//

import UIKit

class HeaderView: UIView {
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var albumName: UILabel!
    @IBOutlet weak var albumDate: UILabel!
    @IBOutlet weak var albumPrice: UILabel!
}
