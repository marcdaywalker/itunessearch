//
//  MasterIpadViewController.swift
//  iTunesSearch
//

import UIKit

class MasterIpadViewController: UICollectionViewController {
    
    var viewModel: MasterIpadViewModel = MasterIpadViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = LocalizableConstants.Search.rawValue.localized()
        self.collectionView?.register(UINib(nibName: "AlbumCollectionCell", bundle: nil), forCellWithReuseIdentifier: "albumCollectionCell")
        viewModel.viewController = self

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
            self.viewModel.search(text: "Marea")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = collectionView?.indexPathsForSelectedItems?.first {
                let controller = segue.destination as! DetailViewController
                controller.viewModel = DetailViewModel(album: viewModel.album(row: indexPath.row))
                controller.viewModel?.viewController = controller
            }
        }
    }
}

// MARK: - Collection View
extension MasterIpadViewController {
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.rows
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "albumCollectionCell", for: indexPath) as? AlbumCollectionCell else {
            return UICollectionViewCell()
        }
        cell.albumLabel?.text = viewModel.albumTitle(row: indexPath.row)
        cell.artistLabel?.text = viewModel.artistTitle(row: indexPath.row)
        cell.albumImage?.imageFromURL(url: viewModel.thumbnail(row: indexPath.row))
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetail", sender: nil)
        self.collectionView?.deselectItem(at: indexPath, animated: true)
    }
}

extension MasterIpadViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        viewModel.search(text: searchBar.text)
        searchBar.resignFirstResponder()
    }
}

