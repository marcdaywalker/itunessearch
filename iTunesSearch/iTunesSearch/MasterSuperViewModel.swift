//
//  MasterSuperViewModel.swift
//  iTunesSearch
//

import Foundation

class MasterSuperViewModel {
    private var albums: [Album] = [Album]()
    
    var rows: Int {
        return albums.count
    }
    
    func search(text: String, completion: @escaping (String?) -> ()) {
        albums = [Album]()
        ServicesManager.search(text: text) { [unowned self] (err, dic) in
            if let msg = err {
                completion(msg)
            } else {
                guard let json = dic else {
                    completion(LocalizableConstants.NoResultsFound.rawValue.localized())
                    return
                }
                
                if let jsonArray = json["results"] as? [[String: AnyObject]] {
                    for json in jsonArray {
                        if let album = self.parseAlbums(json: json) {
                            self.albums.append(album)
                        }
                    }
                    completion(nil)
                } else {
                    completion(LocalizableConstants.NoResultsFound.rawValue.localized())
                }
                
            }
        }
    }
    
    private func parseAlbums (json: [String: AnyObject]) -> Album? {
        guard let artist = json[Album.Keys.artistName] as? String,
            let albumTitle = json[Album.Keys.albumName] as? String,
            let thumbnailString = json[Album.Keys.thumbnail] as? String,
            let thumbnailURL = URL(string: thumbnailString),
            let releaseDateString = json[Album.Keys.releaseDate] as? String,
            let releaseDate = releaseDateString.toDate(),
            let price = json[Album.Keys.collectionPrice] as? NSNumber,
            let code = json[Album.Keys.currencyCode] as? String else {
                return nil
        }
        let album = Album(artistName: artist, albumName: albumTitle, thumbnail: thumbnailURL, releaseDate: releaseDate, price: price, currencyCode: code, tracks: [Track]())
        return album
    }
    
    func albumTitle (row: Int) -> String {
        return albums[row].albumName
    }
    
    func artistTitle (row: Int) -> String {
        return albums[row].artistName
    }
    
    func thumbnail (row: Int) -> URL {
        return albums[row].thumbnail
    }
    
    func album (row: Int) -> Album {
        return albums[row]
    }
}
