//
//  MasterViewController.swift
//  iTunesSearch
//

import UIKit

class MasterViewController: UITableViewController {

    var viewModel: MasterViewModel = MasterViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = LocalizableConstants.Search.rawValue.localized()
        self.tableView.register(UINib(nibName: "AlbumCell", bundle: nil), forCellReuseIdentifier: "albumCell")
        viewModel.viewController = self
        
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
//            self.viewModel.search(text: "Marea")
//        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let controller = segue.destination as! DetailViewController
                controller.viewModel = DetailViewModel(album: viewModel.album(row: indexPath.row))
                controller.viewModel?.viewController = controller
            }
        }
    }

    // MARK: - Table View

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.rows
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "albumCell", for: indexPath) as? AlbumCell else {
            return UITableViewCell()
        }
        cell.albumLabel?.text = viewModel.albumTitle(row: indexPath.row)
        cell.artistLabel?.text = viewModel.artistTitle(row: indexPath.row)
        cell.albumImage?.imageFromURL(url: viewModel.thumbnail(row: indexPath.row))
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetail", sender: nil)
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension MasterViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        viewModel.search(text: searchBar.text)
        searchBar.resignFirstResponder()
    }
}

