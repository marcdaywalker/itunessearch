//
//  MasterViewModel.swift
//  iTunesSearch
//

import Foundation

class MasterViewModel: MasterSuperViewModel {
    weak var viewController: MasterViewController?
    
    func search(text: String?) {
        guard let text = text else { return }
        viewController?.showActivityIndicator()
        super.search(text: text) { (msg) in
            if let msg = msg {
                self.viewController?.showMessage(message: msg)
            } else {
                OperationQueue.main.addOperation({
                    self.viewController?.hideActivityIndicator()
                    self.viewController?.tableView.reloadData()
                })
            }
        }
    }
}
