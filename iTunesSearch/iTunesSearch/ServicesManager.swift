//
//  ServicesManager.swift
//  iTunesSearch
//

import Foundation

struct ServicesManager {
    enum searchType: String {
        case album
        case song
    }
    
    static func search (text: String, type: searchType = .album, completion: @escaping (String?, [String: AnyObject]?) -> ()) {
        guard let escapedString = text.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed),
            let url = URL(string: "https://itunes.apple.com/search?term=\(escapedString)&entity=\(type.rawValue)") else {
            completion(LocalizableConstants.TextStrangeCharacter.rawValue.localized(), nil)
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if let data = data {
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: AnyObject] {
                        completion(nil, jsonResult)
                    } else {
                        completion(LocalizableConstants.NoResultsFound.rawValue.localized(), nil)
                    }
                } catch {
                    completion(LocalizableConstants.NoResultsFound.rawValue.localized(), nil)
                }
            }
        }
        task.resume()
    }
}
