//
//  String.swift
//  iTunesSearch
//

import Foundation

extension String {
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
    func toDate() -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        return formatter.date(from: self)
    }
}

enum LocalizableConstants: String {
    case TextStrangeCharacter
    case NoResultsFound
    case NoDetailsFound
    case TextIsMandatory
    case UnknownAuthor
    case NoDescriptionFound
    case Cancel
    case NotAvailable
    case Search
}
