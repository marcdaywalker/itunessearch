//
//  Track.swift
//  iTunesSearch
//

import Foundation

struct Track {
    let title: String
    let previewURL: URL
    let trackNumber: Int
    
    struct Keys {
        static let previewUrl = "previewUrl"
        static let  trackName = "trackName"
        static let  trackNumber = "trackNumber"
    }
}
