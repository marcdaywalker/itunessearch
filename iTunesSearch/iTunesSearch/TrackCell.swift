//
//  TrackCell.swift
//  iTunesSearch
//

import UIKit

class TrackCell: UITableViewCell {
    @IBOutlet weak var trackNumber: UILabel!
    @IBOutlet weak var trackName: UILabel!
    var trackPreview: URL!
}
