//
//  UIImageView.swift
//  iTunesSearch
//

import UIKit

extension UIImageView {
    func imageFromURL (url: URL) {
        self.image = UIImage(named: "placeholder")
        let task = URLSession(configuration: URLSessionConfiguration.default).dataTask(with: url) { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                DispatchQueue.main.async(execute: {
                    self.image = UIImage(data: data)
                })
            }
        }
        
        // Run task
        task.resume()
    }
}
