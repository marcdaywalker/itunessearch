//
//  ExtensionTest.swift
//  iTunesSearch
//

import XCTest
@testable import iTunesSearch

class ExtensionTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testStringExtension() {
        XCTAssert(LocalizableConstants.Search.rawValue.localized() == "Búsqueda")
        
        let date = Date(timeIntervalSince1970: 100000)
        let dateFromExtension = "1970-01-02T04:46:40Z".toDate()
        XCTAssertNotNil(dateFromExtension)
        guard let date_ = dateFromExtension else {
            XCTFail()
            return
        }
        XCTAssert(date == date_)
    }
    
    func testDateExtension () {
        let date = Date(timeIntervalSince1970: 100000)
        let dateFromExtension = date.dateToMediumString()
        XCTAssert(dateFromExtension == "Jan 2, 1970")
    }
}
