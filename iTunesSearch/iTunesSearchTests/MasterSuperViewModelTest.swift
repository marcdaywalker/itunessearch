
//
//  MasterSuperViewModelTest.swift
//  iTunesSearch
//

import XCTest
@testable import iTunesSearch

class MasterSuperViewModelTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSearchAlbums () {
        let ex = expectation(description: "testSearchAlbums")
        let vm = MasterSuperViewModel()
        vm.search(text: "queen") { msg in
            XCTAssertNil(msg)
            XCTAssert(vm.rows == 49)
            
            XCTAssert(vm.albumTitle(row: 0) == "The Platinum Collection (Greatest Hits I, II & III)")
            XCTAssert(vm.artistTitle(row: 0) == "Queen")
            XCTAssert(vm.thumbnail(row: 0).absoluteString == "http://is3.mzstatic.com/image/thumb/Music1/v4/94/92/a3/9492a374-e6e3-8e92-0630-a5761070b0f7/source/100x100bb.jpg")

            ex.fulfill()
        }
        waitForExpectations(timeout: 15, handler: nil)
    }
}
