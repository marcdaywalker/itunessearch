//
//  ServiceTest.swift
//  iTunesSearch
//

import XCTest
@testable import iTunesSearch

class ServiceTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSearchAlbums () {
        let ex = expectation(description: "testSearchAlbums")
        ServicesManager.search(text: "queen") { (err, dic) in
            XCTAssertNil(err)
            let resultCount = dic?["resultCount"] as? Int
            XCTAssert(resultCount == 50)
            let results = dic?["results"] as? [[String: AnyObject]]
            XCTAssert(results?.count == resultCount)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 15, handler: nil)
    }
}
