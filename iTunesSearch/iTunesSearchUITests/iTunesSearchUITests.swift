//
//  iTunesSearchUITests.swift
//  iTunesSearchUITests
//

import XCTest

class iTunesSearchUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSearch() {
        
        let app = XCUIApplication()
        let searchField = app.tables["Empty list"].children(matching: .searchField).element
        searchField.tap()
        searchField.typeText("queen")
        app.typeText("\r")
        XCTAssertTrue(app.tables.cells.containing(.staticText, identifier:"The Platinum Collection (Greatest Hits I, II & III)").staticTexts["Queen"].exists)
        app.tables.cells.containing(.staticText, identifier:"The Platinum Collection (Greatest Hits I, II & III)").staticTexts["Queen"].tap()
        XCTAssertTrue(app.tables.staticTexts["Jan 1, 2014"].exists)
        XCTAssertTrue(app.tables.staticTexts["$19.99"].exists)
    }
    
}
